----------------------------------------------------

::::::::::::   app birthdayreminder  :::::::::::::::

----------------------------------------------------

+ [Definición:]
	
	Es un app que funciona como Api Rest para consultar si un usuario esta de cumpleaños, entregandole una felicitacion y un poema si o esta.

+ [Detalles de construccion:]
	
	* Java JDK version 11.0.7.
	* Spring Boot, Última versión estable 5.1.6 1.
	* Apache Maven 3.6.3.
 
+ [Instalacion]
	
	* Situarse dentro del directorio apibirthdayreminder
	* Tener instalado Java en la variable de entorno JAVA_HOME  
	* Tener instalado Maven en la variable de entorno M2_HOME 
	
	* Aplicar una serie de comandos en la CMD DOS de Windows o SHELL de Linux.
		
		$ mvn install
		$ mvn clean
		$ mvn test
		$ mvn package
	
	* Desplegar la app en el entorno local.

		$ java -jar target/apibirthdayreminder-0.0.1-SNAPSHOT.jar --spring.datasource.password=XXXXXXXXX

	* Una vez desplegado  esta escuhando por el puerto 8080.

	 NOTA: verificar que ninguna otra aplicacion este escuchando por el puerto 8080

	 * La url  expuesta es: http://localhost:8080/api

	 * Posee dos rutas de consultas GET.

	 	1. Obtiene todos los usuarios que han consultado la app.
	 		
	 		 http://localhost:8080/api/userschecked         

	 	2. Verifica si el usuario esta de cumpleaños, dando dos posibles respuestas.
	 		2.1: No esta de cumpleaños pero le informa los dias que le faltan para cumplir y la edad que tiene.
	 		2.2: Lo felicita por esta de cumpleaños y le dedica un poema.

	 		http://localhost:8080/api/checkbirthday?nombres=Covid Brayan&apellidos=Vega Rodriguez&fecha=13-06-1989





