package com.apibirthdayreminder.api.app.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apibirthdayreminder.api.app.check.ValidateUser;
import com.apibirthdayreminder.api.app.entities.*;
import com.apibirthdayreminder.api.app.entities.services.UserServiceImpl;

@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api", "/home"})
public class CheckBirthdayController{
	
	@Autowired
	ValidateUser validateUser;
	@Autowired
	private UserServiceImpl userServiceRepository;
	
	@GetMapping("/checkbirthday")
	public ResponseEntity<User> checkBirthdayForUser(
			@RequestParam(name = "nombres", required = true) String nombres,
			@RequestParam(name = "apellidos", required = true) String apellidos,
			@RequestParam(name = "fecha", required = true) String fechaNacimiento) {
			User user = null;
			user = validateUser.getValidateUser(nombres, apellidos, fechaNacimiento);
			userServiceRepository.save(user);
			return ResponseEntity.ok(user);
	}
	@GetMapping("/userschecked")
	public ResponseEntity<Iterable<User>> checkBirthdayAllUser() {
			return ResponseEntity.ok(userServiceRepository.findAll());
	}
}
