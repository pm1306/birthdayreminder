package com.apibirthdayreminder.api.app.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity // This tells Hibernate to make a table out of this class
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonInclude(value = JsonInclude.Include.NON_EMPTY, content = JsonInclude.Include.NON_NULL)
public class User implements Serializable {

	private static final long serialVersionUID = -6717223592955526408L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	@Setter
	private Integer id;
	@Getter
	@Setter
	private String nombre;
	@Getter
	@Setter
	private String apellido;
	@Getter
	@Setter
	private Integer edad;
	@Getter
	@Setter
	private Integer faltantes;
	@Getter
	@Setter
	private String poema;
	@Getter
	@Setter
	private String felicitaciones;
	@Getter
	@Setter
	private String fecha;
}