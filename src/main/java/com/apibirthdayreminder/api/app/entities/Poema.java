package com.apibirthdayreminder.api.app.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Poema implements Serializable {
	private static final long serialVersionUID = 1344974307432836570L;
	@Getter @Setter private String title;
	@Getter @Setter private String content;
	@Getter @Setter private String url;
	@Getter @Setter private Poet poet;

}
