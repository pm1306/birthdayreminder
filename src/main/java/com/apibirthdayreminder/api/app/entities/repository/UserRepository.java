package com.apibirthdayreminder.api.app.entities.repository;

import org.springframework.data.repository.CrudRepository;

import com.apibirthdayreminder.api.app.entities.User;

//This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
//CRUD refers Create, Read, Update, Delete
//  Only lines
public interface UserRepository extends CrudRepository<User, Integer>{

}
