package com.apibirthdayreminder.api.app.entities.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.apibirthdayreminder.api.app.entities.User;
import com.apibirthdayreminder.api.app.entities.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository usuario;
	
	@Override
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return  (List<User>) usuario.findAll();
	}

	@Override
	public void save(User user) {
		usuario.save(user);
	}

}
