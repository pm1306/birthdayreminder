package com.apibirthdayreminder.api.app.entities.services;

import java.util.List;

import com.apibirthdayreminder.api.app.entities.User;

public interface UserService {

	public List<User> findAll();
	public void  save(User user);
} 
