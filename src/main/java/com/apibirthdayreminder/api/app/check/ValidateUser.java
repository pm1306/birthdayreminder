package com.apibirthdayreminder.api.app.check;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.apibirthdayreminder.api.app.entities.Poema;
import com.apibirthdayreminder.api.app.entities.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ValidateUser {
	
	private static final String FORMATO_FECHA="dd-MM-yyyy";
	private static final String FORMATO_FECHA_CORTA="dd/MM/yy";
	private static final String URL_API="https://www.poemist.com/api/v1/randompoems";
	private static final String FELICITACION="Felicidades por tu cumpleaños, que tus deseos se cumplan y que la vida te regale muchos momentos de felicidad.";
	
	private static final long FLAG_FECHA=-1;
	@Autowired
	private RestTemplate restTemplate;
	
	public User getValidateUser(String nombres, String apellidos, String fecha) {
		User userCheck = new User();
		userCheck.setNombre(getFirstNombre(nombres));
		userCheck.setApellido(getFirstApellido(apellidos));
		userCheck.setFecha(getFecha(fecha));
		userCheck.setEdad(getEdad(fecha));
		if (FLAG_FECHA==getDiasFaltantes(fecha)) {
			userCheck.setFelicitaciones(FELICITACION);
			userCheck.setPoema(getPoema());			
		} else {
			userCheck.setFaltantes(getDiasFaltantes(fecha));
		}
		return userCheck;
	}
	private String getPoema() {
		String poema = "";
		try {
			ResponseEntity<List<Poema>> response = restTemplate.exchange(URL_API, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<Poema>>() {
					});
			poema = null != response.getBody() ? getPoemasOfPage(response.getBody().get(0)) : " Not Found poema";
		} catch (RestClientException e) {
			log.info("Error API POEMA" + e.getLocalizedMessage());
		}
		return poema;
	}
	

	private String getPoemasOfPage(Poema poema) {
		return poema.getTitle().concat(".    \n\""+poema.getContent().concat("\""));
	}

	private Integer getDiasFaltantes(String fechaNacimiento) {
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern(FORMATO_FECHA);
		LocalDate fechaNac = LocalDate.parse(fechaNacimiento, fmt);
		LocalDate hoy = LocalDate.now();
		LocalDate nextBDay = fechaNac.withYear(hoy.getYear());
		if (nextBDay.isBefore(hoy) || nextBDay.isEqual(hoy)) {
			nextBDay = nextBDay.plusYears(1);
		}
		Integer totalDias = (int) ChronoUnit.DAYS.between(hoy, nextBDay);
		return totalDias == 365 ? -1:totalDias;
	}
	
	private String getFecha(String fechaNacimiento) {
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern(FORMATO_FECHA);
		LocalDate fechaNac = LocalDate.parse(fechaNacimiento, fmt);
		DateTimeFormatter fmts = DateTimeFormatter.ofPattern(FORMATO_FECHA_CORTA);
		return fechaNac.format(fmts);
	}

	private String getFirstNombre(String nombres) {
		nombres = nombres.trim();
		String[] nombresList = nombres.split(" ");
		return nombresList[0];
	}

	private String getFirstApellido(String apellidos) {
		apellidos = apellidos.trim();
		String[] apellidosList = apellidos.split(" ");
		return apellidosList[0];
	}

	private Integer getEdad(String fechaNacimiento) {
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern(FORMATO_FECHA);
		LocalDate fechaNac = LocalDate.parse(fechaNacimiento, fmt);
		LocalDate hoy = LocalDate.now();
		Period periodo = Period.between(fechaNac, hoy);
		return periodo.getYears();
	}
}