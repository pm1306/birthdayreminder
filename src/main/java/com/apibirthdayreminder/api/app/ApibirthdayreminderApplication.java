package com.apibirthdayreminder.api.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import lombok.Generated;

@SpringBootApplication
public class ApibirthdayreminderApplication {
	
	@Generated
	public static void main(String[] args) {
		SpringApplication.run(ApibirthdayreminderApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}
