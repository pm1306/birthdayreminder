package com.apibirthdayreminder.api.app.check;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.apibirthdayreminder.api.app.entities.Poema;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class ValidateUserTest {
		 
    @InjectMocks
    private ValidateUser validateUser;
    
    private String nombres, apellidos, fecha, url;
    private List<Poema> poemaList;
    
    @Mock
    private RestTemplate restTemplate;
    
    private static final String RS_API_POEMA= "src/test/resources/RS_API_POEMA.json";
    
    @Before
    public void before() throws JsonParseException, JsonMappingException, FileNotFoundException, IOException {
    	 ObjectMapper mapper = new ObjectMapper();
    	url = "https://www.poemist.com/api/v1/randompoems";
    	nombres="Pablo Jesus";
    	apellidos="Montaño Roa";
    	fecha="12-06-1989";
    	poemaList = mapper.readValue(new FileInputStream(RS_API_POEMA), new TypeReference<List<Poema>>(){});
    }
    
    @Test
    public void getValidateUserTest(){
    	validateUser.getValidateUser(nombres, apellidos, fecha);
    	Assert.assertNotNull(validateUser);
    }
    
    @Test
    public void getValidateUserTestCumple(){
    	DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    	LocalDate hoy = LocalDate.now();
    	this.fecha =fmt.format(hoy);
    	ResponseEntity<List<Poema>> response = new ResponseEntity<List<Poema>>(HttpStatus.ACCEPTED);
    	Mockito.when(restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Poema>>() {
				})).thenReturn(response);
    	validateUser.getValidateUser(nombres, apellidos, fecha);
    	Assert.assertNotNull(validateUser);
    }
    
    @Test
    public void getValidateUserTestResponse()  throws Exception{
    	DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    	LocalDate hoy = LocalDate.now();
    	this.fecha =fmt.format(hoy);
    	ResponseEntity<List<Poema>> response = new ResponseEntity<List<Poema>>(poemaList, HttpStatus.ACCEPTED);
    	
    	Mockito.when(restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Poema>>() {
				})).thenReturn(response);
    	validateUser.getValidateUser(nombres, apellidos, fecha);
    	Assert.assertNotNull(validateUser);
    }
    
//    @Test
//    public void getValidateUserTestNotPage()  throws Exception{
//    	thrown.expect(RestClientException.class);
//    	DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd-MM-yyyy");
//    	LocalDate hoy = LocalDate.now();
//    	this.fecha =fmt.format(hoy);
//    	this.url="https://www.poemist.com/api/v1/randompoems/pablo";
//    	ResponseEntity<List<Poema>> response = new ResponseEntity<List<Poema>>(poemaList, HttpStatus.NOT_FOUND);
//    	Mockito.when(restTemplate.exchange(url, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<Poema>>() {
//    		})).thenReturn(response);
//    	Assert.assertNull("Not Found poema", validateUser.getValidateUser(nombres, apellidos, fecha).getPoema());
//    }
}
